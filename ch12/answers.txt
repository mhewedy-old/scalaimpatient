1: [Write a function values(fun: (Int) => Int, low: Int, high: Int) that yields a
collection of function inputs and outputs in a given range. For example,
values(x => x * x, -5, 5) should produce a collection of pairs (-5, 25), (-4, 16),
(-3, 9),  . . . , (5, 25).]

object Main extends App {
  def values(fun: (Int) => Int, low: Int, high: Int) = {
    (low to high) zip (low to high).map(fun)
  }
  
  println(values(x => x * x, -5, 5))
}

2: [How do you get the largest element of an array with reduceLeft?]

object Main extends App {
  // Or we could have used math.max...
  def max(x: Int, y: Int) : Int = {
    if (x > y)
      x
    else
      y
  }
  
  val array = Array(1, 200, 3, 77, 7, 2, 3, 10, 100)
  
  println(array.reduceLeft(max))
}

3: [Implement the factorial function using to and reduceLeft, without a loop or
recursion.]

object Main extends App {
  def factorial(fac: Int) : Int = {
    (fac to (1, -1)).reduceLeft(_ * _) 
  }
  
  println(factorial(5))
}

4: [The previous implementation needed a special case when n < 1. Show how
you can avoid this with foldLeft. (Look at the Scaladoc for foldLeft. It’s like
reduceLeft, except that the ﬁrst value in the chain of combined values is supplied
in the call.)]

object Main extends App {
  def factorial(fac: Int) : Int = {
    (fac to (1, -1)).foldLeft(1)(_ * _) 
  }
  
  println(factorial(0))
}

5: [Write a function largest(fun: (Int) => Int, inputs: Seq[Int]) that yields the largest value of a function within a given sequence of inputs. For example,
largest(x => 10 * x - x * x, 1 to 10) should return 25. Don’t use a loop or
recursion.]

object Main extends App {
  def largest(fun: (Int) => Int, inputs: Seq[Int]) = {
    inputs.map(fun).max
  }
  
  println(largest(x => 10 * x - x * x, 1 to 10))
}

6: [Modify the previous function to return the input at which the output is largest. For example, largestAt(fun: (Int) => Int, inputs: Seq[Int]) should return 5. Don’t use a loop or recursion.]

object Main extends App {
  def largest(fun: (Int) => Int, inputs: Seq[Int]) = {
    inputs.zipWithIndex.map(i => (i._1, fun(i._1))).maxBy(_._2)._1
  }
  
  println(largest(x => 10 * x - x * x, 1 to 10))
}

7: [It’s easy to get a sequence of pairs, for example val pairs = (1 to 10) zip (11 to 20) Now suppose you want to do something with such a sequence—say, add up
the values. But you can’t do
pairs.map(_ + _)
The function _ + _ takes two Int parameters, not an (Int, Int) pair. Write a
function adjustToPair that receives a function of type (Int, Int) => Int and
returns the equivalent function that operates on a pair. For example,
adjustToPair(_ * _)((6, 7)) is 42.
Then use this function in conjunction with map to compute the sums of the
elements in pairs.]

object Main extends App {
  def adjustToPair(fun: (Int, Int) => Int) : ((Int, Int)) => Int = {
    case(x, y) => fun(x, y)
  }
  
  println(adjustToPair(_ * _)((6, 7)))
  
  val pairs = (1 to 10) zip (11 to 20)
  println(pairs.map(adjustToPair(_ + _)))
}

8: [In Section 12.8, “Currying,” on page 149, you saw the corresponds method used
with two arrays of strings. Make a call to corresponds that checks whether the
elements in an array of strings have the lengths given in an array of integers.]

object Main extends App {
  val c = Array("Goodbye!", "Cruel!", "World!")
  val d = Array("Goodbye!".size, "Cruel!".size, "World!".size) 
  val e = Array(2, 19, 9) 
  
  println(c.corresponds(d)(_.size == _))
  println(c.corresponds(e)(_.size == _))
}

9: [Implement corresponds without currying. Then try the call from the preceding
exercise. What problem do you encounter?]

object Main extends App {
  def corresponds(lhs: Seq[String], rhs: Seq[Int], p: (String, Int) => Boolean): Boolean = {
    (lhs zip rhs).map(t => p(t._1, t._2)).max
  }
  
  val c = Array("Goodbye!", "Cruel!", "World!")
  val d = Array("Goodbye!".size, "Cruel!".size, "World!".size) 
  val e = Array(2, 19, 9) 
  
  println(corresponds(c, d, _.size == _))
  println(corresponds(c, e, _.size == _))
}

// We can achieve the same effect, but we have to specify the exact type information for both "A" and "B" when declaring the method, and the syntax is a little nastier, and we can't use p(_, _) in the implementation

10: [Implement an unless control abstraction that works just like if, but with an
inverted condition. Does the ﬁrst parameter need to be a call-by-name
parameter? Do you need currying?]

object Main extends App {
  def notif(condition: => Boolean)(block: => Unit) {
    if (!condition) {
      block
    }
  }
  
  def notifnocurry(condition: => Boolean, block: => Unit) {
    if (!condition) {
      block
    }
  }
  
  def notifnonamedparam(condition: () => Boolean)(block: => Unit) {
    if (!condition()) {
      block
    }
  }
  
  var x = 5
  if (x != 5) {
    println("Shouldn't see this")
  }
  else {
    println("Should see this")
  }
  
  notif (x != 5) {
    println("Should see this too, and with nice syntax")
  }
  
  notifnocurry (x != 5, { println("Should see this too, but nastier syntax") })
  
  notifnonamedparam (() => x != 5) {
    println("Should see this too, but still nasty syntax")
  }
}

