1: [Write an example program to demonstrate that
package com.horstmann.impatient
is not the same as
package com
package horstmann
package impatient]

package com {
  class Inner

  package horstmann {
    package impatient {
      // This works, we can see members of com
      class Person(inner: Inner)
    }
  }
}

package com.horstmann.impatient {
  // This doesn't work - we can't see members of com
  class PersonTwo(inner: Inner)
}

2: [Write a puzzler that bafﬂes your Scala friends, using a package com that isn’t at the top level.]

package impatient {
  package horstmann {
    // Not sure what the question is actually asking for...but
    package com {
      class Test
    }
  }
}

object Main extends App {
  val test : impatient.horstmann.com.Test = new impatient.horstmann.com.Test
}

3: [Write a package random with functions nextInt(): Int, nextDouble(): Double, and
setSeed(seed: Int): Unit. To generate random numbers, use the linear
congruential generator
next = previous × a + b mod 2n,
where a = 1664525, b = 1013904223, and n = 32.]

package object random {
  private[random] var prev = 0;
    
  def nextInt: Int = {
    val next = (prev * 1664525) + (1013904223 % math.pow(2, 32).toInt)
    prev = next
    next
  }
  
  def nextDouble: Double = {
    nextInt.toDouble
  }
  
  def setSeed(seed: Int) {
    prev = seed
  }
}

object Main extends App {
  random.setSeed(0)
  val randomInt = random.nextInt
  println(randomInt)
  val randomDouble = random.nextDouble
  println(randomDouble)
  val randomInt2 = random.nextInt
  println(randomInt2)
  val randomDouble2 = random.nextDouble
  println(randomDouble2)
}

4: [Why do you think the Scala language designers provided the package object syntax instead of simply letting you add functions and variables to a package?]

This is due to a JVM limitation in that functions or variables cannot directly be added to packages.

5: [What is the meaning of private[com] def giveRaise(rate: Double)? Is it useful?]

It means a private function to the com namespace that takes in a double argument called rate. It is useless as it does not return any value (it returns Unit).

6: [Write a program that copies all elements from a Java hash map into a Scala hash map. Use imports to rename both classes.]

import scala.collection.JavaConversions._
import java.util.{ HashMap => JavaHashMap }
import scala.collection.mutable.{ HashMap => ScalaHashMap }

object Main extends App {
  var javaHm = new JavaHashMap[java.lang.String, java.lang.String]

  javaHm.put("PostgreSQL", "Free Open Source Enterprise Database");
  javaHm.put("DB2", "Enterprise Database , It's expensive");
  javaHm.put("Oracle", "Enterprise Database , It's expensive");
  javaHm.put("MySQL", "Free Open SourceDatabase");

  val scalaHm = new ScalaHashMap[String, String]

  scalaHm.putAll(javaHm)

  println(scalaHm)
}

7: [In the preceding exercise, move all imports into the innermost scope possible.]

object Main extends App {
  import java.util.{ HashMap => JavaHashMap }
  var javaHm = new JavaHashMap[java.lang.String, java.lang.String]

  javaHm.put("PostgreSQL", "Free Open Source Enterprise Database");
  javaHm.put("DB2", "Enterprise Database , It's expensive");
  javaHm.put("Oracle", "Enterprise Database , It's expensive");
  javaHm.put("MySQL", "Free Open SourceDatabase");

  import scala.collection.mutable.{ HashMap => ScalaHashMap }
  val scalaHm = new ScalaHashMap[String, String]

  import scala.collection.JavaConversions._
  scalaHm.putAll(javaHm)

  println(scalaHm)
}

8: [What is the effect of
import java._
import javax._
Is this a good idea?]

These two statements import the entire packages java and javax, which, collectively, contain most of the classes/packages in the entire JRE. This is not a good idea, as it has the potential to introduce naming conflicts in client code.

9: [Write a program that imports the java.lang.System class, reads the user name from the user.name system property, reads a password from the Console object, and prints a message to the standard error stream if the password is not "secret". Otherwise, print a greeting to the standard output stream. Do not use
any other imports, and do not use any qualiﬁed names (with dots).]

import java.lang.System

object Main extends App {
  val userName = System.getProperty("user.name")

  val password = System.console.readLine.toString
  
  if (password != "secret")
    println("Password is not secret!")
  else
    println("Welcome to the system!")
}

10: [Apart from StringBuilder, what other members of java.lang does the scala package override?]

Boolean
Byte
Float
Double
Long
Number
Math
Process
Short
